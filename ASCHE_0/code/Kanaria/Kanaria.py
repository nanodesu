# -*- coding: utf8 -*-
import dcpp_client
import services

###############################################
# START 
###############################################
print "###############################################"
print "# dcpp_client v0.0.2                          #"
print "# (c) ASCHE 2008                              #"
print "###############################################"
###############################################
host     = "avangard.no-ip.org"
port     = 411
codepage = "cp1251"
###############################################
login    = "Kanaria"
password = None
descr    = u"Kanaria v0.0.2 Теперь в юникоде ^_^"
owner    = "ASCHE_0"

###############################################
client = dcpp_client.DCPP_CLIENT()

print "Connecting to %s: %s..." %(host, port)

if not client.connect((host, port), codepage, login, password, descr):
    print "Error"
    exit()
print "Done"
print "###############################################"

###############################################
room_list = []

run = 1
while run:
    client.update_cache()

    while 1:
        user, msg = client.process_cache()

        if not user:
            break
        else:
            #owner commands
            if user == owner:
                #quit
                if msg == "q!":
                    client.send_to_user(user, "bye")
                    run = 0
                    break
                #commands
                elif msg[:1] == "+":
                    client.send_to_user("#Main", msg)
                    continue
                #chat
                elif msg[:1] == "*":
                    p = msg.find(" ")
                    if p != -1:
                        user_to = msg[1:p]
                        msg_to  = msg[p+1:]
                        client.send_to_user(user_to, msg_to)
                        continue

            #ping
            if msg == "->ping":
                client.send_to_user(user, "<-pong")
            #help
            elif msg == "->help":
                client.send_to_user(user, services.GetHelp())

            #weather
            elif msg == "->weather":
                client.send_to_user(user, services.GetWeather())

            #quotes
            elif msg == "->bash":
                client.send_to_user(user, services.GetBashQuote())
            elif msg == "->nyash":
                client.send_to_user(user, services.GetNyashQuote())
            elif msg == "->takto":
                client.send_to_user(user, services.GetTaktoQuote())

            #decode last
            elif (msg == "->en") or (msg == "->ru"):
                for room in room_list:
                    user_room, last_msg = room
                    if user_room == user:
                        client.send_to_user(user, services.Decode(last_msg, msg[2:]))

            #decode
            elif (msg[:4] == "->en") or (msg[:4] == "->ru"):
                client.send_to_user(user, services.Decode(msg[5:], msg[2:4]))

            #---------------------------------------------#
            #last_msg
            else:
                flag = 0

                for i in range(len(room_list)):
                    user_room, last_msg = room_list[i]
                    if user_room == user:
                        room_list[i] = user, msg
                        flag = 1
                        break

                if not flag:
                    new_room = user, msg
                    room_list.append(new_room)

###############################################
print "Exiting..."

del client

print "Done"
print "###############################################"
