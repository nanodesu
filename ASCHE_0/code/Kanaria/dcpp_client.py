import socket, select

###############################################
# DCPP_CLIENT
###############################################
class DCPP_CLIENT:
    ST_DISCONNECTED = 0
    ST_CONNECTED = 1

    #---------------------------------------------#
    # constructor 
    #---------------------------------------------#
    def __init__(self):
        self._codepage = None
        self._login    = None

        self._cache    = ""

        self._state    = DCPP_CLIENT.ST_DISCONNECTED
        self._socket   = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    #---------------------------------------------#
    # destructor 
    #---------------------------------------------#
    def __del__(self):
        self.disconnect()

    #---------------------------------------------#
    # connect
    #---------------------------------------------#
    def connect(self, address, codepage, login, password, descr):
        if self.is_connected(): return None # if already connected

        if not login: return None # if empty login

        self._login    = login
        self._codepage = codepage
        if codepage != "cp1251": codepage = "cp1251"

        try:
            self._socket.connect(address)
            self._state = DCPP_CLIENT.ST_CONNECTED
        except:
            return None
        
        #print self._recv()
        # TODO: process this!

        key = _create_key(u"EXTENDEDPROTOCOL_verlihub")
        if not key: return None

        self._send(u"$Key %s|$ValidateNick %s|" %(key, self._login))
        #print self._recv()
        # TODO: process this!

        if password:
            self._send(u"$Version 1,0091|$MyPass %s|$MyINFO $ALL %s %s$ $0$$0$|" %(password, self._login, descr))
        else:
            self._send(u"$Version 1,0091|$MyINFO $ALL %s %s$ $0$$0$|" %(self._login, descr))
        #print self._recv()
        # TODO: process this!

        return 1

    #---------------------------------------------#
    # disconnect
    #---------------------------------------------#
    def disconnect(self):
        if not self.is_connected(): return None # if already disconnected

        self._socket.close()
        self._state = DCPP_CLIENT.ST_DISCONNECTED

    #---------------------------------------------#
    # is_connected
    #---------------------------------------------#
    def is_connected(self):
        return (self._state == DCPP_CLIENT.ST_CONNECTED)

    #---------------------------------------------#
    # _recv
    #---------------------------------------------#
    def _recv(self):
        if not self.is_connected(): return None # if disconnected

        inputready, outputready, exceptready = select.select([self._socket, ],[], [], 0.5)
        if inputready:
            try:
                data = self._socket.recv(1024)
                return unicode(data, self._codepage)
            except:
                return None
                # TODO: process this!
        else:
            return None

    #---------------------------------------------#
    # update_cashe
    #---------------------------------------------#
    def update_cache(self):
        data = self._recv()
        if data:
            self._cache += data

    #---------------------------------------------#
    # process_cashe
    #---------------------------------------------#
    def process_cache(self):
        pipepos = self._cache.find("|")
        
        if pipepos != -1:
            data = self._cache[:pipepos]
            self._cache = self._cache[pipepos+1:]

            #PM
            if data.find("$To:") != -1:
                p1 = data.find("From: ") + 6
                p2 = data.find(" $")
                user = data[p1:p2]

                p1 = data.find("> ") + 2
                msg = data[p1:]
            #MAIN
            elif data.find("> ") != -1:
                user = "#Main"

                p1 = data.find("> ") + 2
                msg = data[p1:]
            #OTHER
            else:
                return None, None

            return user, msg
        else:
            return None, None

    #---------------------------------------------#
    # _send
    #---------------------------------------------#
    def _send(self, data):
        if not self.is_connected(): return None # if disconnected

        try:
            data = unicode(data).encode(self._codepage, "replace")
            self._socket.send(data)
        except:
            return None
            # TODO: process this!

    #---------------------------------------------#
    # send_to_user
    #---------------------------------------------#
    def send_to_user(self, user, msg):
        if (not user) or (not msg): return None

        # PM
        if user != "#Main":
            self._send(u"$To: %s From: %s $<%s> %s|" %(user, self._login, self._login, msg))
        # MAIN
        else:
            self._send(u"<%s> %s|" %(self._login, msg))

###############################################
# _create_key
###############################################
def _create_key(lock):
    if not lock: return None

    key = {}

    for i in xrange(1, len(lock)):
        key[i] = ord(lock[i]) ^ ord(lock[i-1])

    key[0] = ord(lock[0]) ^ ord(lock[len(lock)-1]) ^ ord(lock[len(lock)-2]) ^ 5

    for i in xrange(0, len(lock)):
        key[i] = ((key[i]<<4) & 240) | ((key[i]>>4) & 15)

    out = ''
    for i in xrange(0, len(lock)):
        out += unichr(key[i])

    out = out.replace(u'\0', u'/%DCN000%/').replace(u'\5', u'/%DCN005%/').replace(u'\44', u'/%DCN036%/')
    out = out.replace(u'\140', u'/%DCN096%/').replace(u'\174', u'/%DCN124%/').replace(u'\176', u'/%DCN126%/')

    return out
