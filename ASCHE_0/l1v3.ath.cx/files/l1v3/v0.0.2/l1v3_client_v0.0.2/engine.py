from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from time import *
from math import *
# my modules
import config
import client

###############################################
# Settings
###############################################
tmr = tmr2 = clock()
fps = 0

#---------------------------------------------# 
move = step = Angle_x = Angle_y = 0

x =   0.0; lx =  0.0
y =   0.0; ly =  0.0
z =  23.0; lz = -1.0 
#---------------------------------------------# 

###############################################
# ChangeView
###############################################
def ChangeView():
    global x, y, z, lx, ly, lz

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    gluLookAt(x,      y,      z,      # Where
              x + lx, y + ly, z + lz, # LookTo
              0.0,    1.0,    0.0)    # Vector

###############################################
# InitGL
###############################################
def InitGL(Width, Height):
    glClearColor(0.0, 0.0, 0.0, 0.0)
    glClearDepth(1.0)
    glDepthFunc(GL_LESS)
    glEnable(GL_DEPTH_TEST)
    glShadeModel(GL_FLAT)
    glEnable(GL_LIGHTING)
    glEnable(GL_LIGHT0)
    glEnable(GL_COLOR_MATERIAL)
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, float(Width) / float(Height), 0.1, 1000.0)
    ChangeView()

    glutWarpPointer(100, 100)

###############################################
# ResizeGLScene
###############################################
def ResizeGLScene(Width, Height):
    if Height == 0: Height = 1

    glViewport(0, 0, Width, Height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, float(Width) / float(Height), 0.1, 1000.0)
    ChangeView()

###############################################
# KeyPressed
###############################################
def KeyPressed(*args):
    global move, step

    # Exit 
    if args[0] == "q":
        if client.connection: client.DisconnectFromServer()
        sys.exit()

    # WSAD
    if   args[0] == "w":
        move = 1
    elif args[0] == "s":
        move = -1
    elif args[0] == "a":
        step = -1
    elif args[0] == "d":
        step = 1

    # JK
    elif args[0] == "j":
        if not client.connection: client.ConnectToServer(config.username)
    elif args[0] == "k":
        if client.connection: client.DisconnectFromServer()

###############################################
# KeyReleased
###############################################
def KeyReleased(*args):
    global move, step

    if   (args[0] == "w") or (args[0] == "s"):
        move = 0
    elif (args[0] == "a") or (args[0] == "d"):
        step = 0

###############################################
# MouseMoved
###############################################
def MouseMoved(*args):
    global Angle_x, Angle_y, lx, ly, lz

    Angle_x += args[0] - 100
    Angle_y += args[1] - 100

    rad_angle = ((Angle_x / 10) % 360) * pi / 180
    lx = sin(rad_angle)
    lz = -cos(rad_angle)

    rad_angle = ((Angle_y / 10) % 360) * pi / 180
    ly = -sin(rad_angle)
    
    ChangeView()

###############################################
# Move
###############################################
def Move():
    global x, z, lx, lz
    global move, step

    if move:
        x = x + move * lx * 0.03
        z = z + move * lz * 0.03

    if step:
        x = x - step * lz * 0.03
        z = z + step * lx * 0.03

    ChangeView()

###############################################
# Idle
###############################################
def Idle():
    global tmr, tmr2, fps
    global move
    global x, y, z, lx, lz

    fps += 1
    elapsed_time = glutGet(GLUT_ELAPSED_TIME)

    fps_time = elapsed_time - tmr
    if fps_time >= 1000: 
        glutSetWindowTitle("FPS = %.2f CONN = %i" %((fps * fps_time / 1000.0), client.connection))
        fps  = 0
        tmr = elapsed_time

        if client.connection:
            str = "%msg%:%coords%:" + "%.4f" %(x + lx) + ":" + "%.4f" %(y - 0.5) + ":" + "%.4f" %(z + lz)
            client.SendToServer(str)

    rot_time = elapsed_time - tmr2
    if rot_time >= 100:
        #---------------------------------------------#
        if client.connection:
            #receive
            msg = client.ReceiveFromServer()
            if msg:
                str = msg.split(":", 1)

                if str[0] == "%reg%":
                    client.addUser(str[1])

                elif str[0] == "%exit%":
                    client.removeUser(str[1])

                elif str[0] == "%msg%":
                    username, comm = str[1].split(":", 1)

                    command, coords = comm.split(":", 1)
                    if command == "%coords%":
                        try:
                            cor = coords.split(":")
                            myx = float(cor[0])
                            myy = float(cor[1])
                            myz = float(cor[2])
                            client.changeCoords(username, myx, myy, myz)
                        except:
                            pass
        #---------------------------------------------#
        glutWarpPointer(100, 100)
        tmr2 = elapsed_time

    if move or step: Move()

    DrawGLScene()

###############################################
# DrawGLScene
###############################################
def DrawGLScene():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    #terrain
    glColor4f(1.0, 1.0, 1.0, 1.0)
    glBegin(GL_QUADS)
    glNormal3f(0, 1, 0)
    glTexCoord2f(0, 1); glVertex3f(-20, -1, -20)
    glTexCoord2f(0, 0); glVertex3f(-20, -1,  20)
    glTexCoord2f(1, 0); glVertex3f( 20, -1,  20)
    glTexCoord2f(1, 1); glVertex3f( 20, -1, -20)
    glEnd()
    #end

    #cube
    glPushMatrix()
    glTranslatef(x + lx, y - 0.5, z + lz)
    glColor4f(1.0, 0.0, 0.0, 1.0)
    glutSolidSphere(0.3, 10, 5)
    glPopMatrix()
    #end

    #users
    for user in client.userlist:
        glPushMatrix()
        glTranslatef(user.x, user.y, user.z)
        glColor4f(1.0, 0.0, 0.0, 1.0)
        glutSolidSphere(0.3, 10, 5)
        glPopMatrix()
    #end

    glutSwapBuffers()
