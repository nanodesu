from OpenGL.GLUT import *
# my modules
import engine
import config

###############################################
# Settings
###############################################
config.LoadSettings("config/config.cfg")

###############################################
# Start
###############################################
glutInit(sys.argv)
glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH)
glutInitWindowSize(config.Width, config.Height)
glutInitWindowPosition(0, 0)
glutCreateWindow("l1v3")

#binding functions
glutDisplayFunc(engine.DrawGLScene)
glutIdleFunc(engine.Idle)
glutReshapeFunc(engine.ResizeGLScene)
#-keyboard
glutKeyboardFunc(engine.KeyPressed);
glutKeyboardUpFunc(engine.KeyReleased);
glutIgnoreKeyRepeat(1);
#-mouse
glutPassiveMotionFunc(engine.MouseMoved);
#end

#start
engine.InitGL(config.Width, config.Height)
glutMainLoop()
#end
