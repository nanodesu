import ConfigParser

###############################################
# Default settings
###############################################
Width  = 800
Height = 600

username = "Guest"

###############################################
# LoadSettigns
###############################################
def LoadSettings(path):
    global Width, Height, username

    config = ConfigParser.ConfigParser()
    config.read(path)

    try: Width = int(config.get("main", "Width"))
    except: pass
    try: Height = int(config.get("main", "Height"))
    except: pass
    try: username = config.get("user", "username")
    except: pass
