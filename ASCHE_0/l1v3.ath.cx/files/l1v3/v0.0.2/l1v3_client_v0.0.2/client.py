import socket

###############################################
# Settings
###############################################
host = "l1v3.ath.cx"
port = 3113
sock = None
connection = 0

userlist = []

###############################################
# User
###############################################
class User:
    def __init__(self):
        self.username = ""
        self.x = 0.0
        self.y = -0.5
        self.z = 22

###############################################
# addUser
###############################################
def addUser(username):
    user = User()
    user.username = username
    userlist.append(user)

###############################################
# removeUser
###############################################
def removeUser(username):
    for user in userlist:
        if user.username == username:
            userlist.remove(user)
            break

###############################################
# changeCoords
###############################################
def changeCoords(username, x, y, z):
    for user in userlist:
        if user.username == username:
            user.x = x
            user.y = y
            user.z = z
            break

###############################################
# ConnectToServer
###############################################
def ConnectToServer(username):
    global host, port, sock, connection

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        sock.connect((host, port))
        sock.setblocking(0)
    except:
        return

    connection = 1
    SendToServer("%reg%:" + username)

###############################################
# SendToServer
###############################################
def SendToServer(msg):
    global sock

    try:
        sock.send(msg)
    except:
        pass

###############################################
# ReceiveFromServer
###############################################
def ReceiveFromServer():
    global sock

    try:
        return sock.recv(1024)
    except:
        pass

###############################################
# DisconnectFromServer
###############################################
def DisconnectFromServer():
    global sock, connection

    for user in userlist:
        userlist.remove(user)

    SendToServer("%exit%")
    sock.close()
    connection = 0
