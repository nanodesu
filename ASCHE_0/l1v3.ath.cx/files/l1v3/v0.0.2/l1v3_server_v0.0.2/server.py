from socket import *
import time	

###############################################
# Settings
###############################################
host = ""
port = 3113

done = 0
userlist = []

#---------------------------------------------#
#- states ------------------------------------#
#---------------------------------------------#
sNotReg = 0
sReg = 1
#---------------------------------------------#

###############################################
# User
###############################################
class User:
    def __init__(self):
        self.addr = ""
        self.sock = None
        self.state = sNotReg
        self.username = ""

    def handleMsg(self, msg):
        global userlist

        str = msg.split(":", 1)

        #sNotReg
        if self.state == sNotReg:
            if str[0] == "%reg%": 
                if str[1]:
                    flag = 1
                    for user in userlist:
                        if str[1] == user.username: flag = 0; break
                    if flag:
                        self.username = str[1]
                        self.state = sReg

                        for user in userlist:
                            if (user.username <> self.username) and user.username:
                                self.sock.send("%reg%:" + user.username)

                        BroadCast(self.username, "%reg%:" + self.username)
        #sReg
        else:
            if msg == "%exit%":
                self.disconnect("exit")

            elif str[0] == "%msg%":
                BroadCast(self.username, "%msg%:" + self.username + ":" + str[1])

    def disconnect(self, reason):
        self.sock.close()
        userlist.remove(self)
        print self.addr + " has disconnected (" + reason + ")"
        BroadCast(self.username, "%exit%:" + self.username)

###############################################
# BroadCast
###############################################
def BroadCast(username, msg):
    for user in userlist:
        if (user.state == sReg) and (user.username <> username):
            try:
                size = user.sock.send(msg)
            except:
                user.disconnect("timeout")

###############################################
# GetNewConnection
###############################################
def GetNewConnection():
    try:
        sock, addr = srv.accept()
    except:
        return None

    sock.setblocking(0)

    user      = User();
    user.sock = sock
    user.addr = addr[0]

    return user

###############################################
###############################################
# Start #######################################
###############################################
###############################################
print "###############################################"
print "# l1v3 server v0.0.2                          #"
print "# (c) Time Studios 2008                       #"
print "###############################################"

srv = socket(AF_INET, SOCK_STREAM)
srv.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)

#---------------------------------------------#
#- binding -----------------------------------#
#---------------------------------------------#
print "%i binding..." %(port)
try:
    srv.bind((host, port))
except:
    print "Error"
    print "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    exit()
print "Done"
#---------------------------------------------#

srv.setblocking(0)

srv.listen(5)
print "Waiting for connections..."
print "###############################################"

###############################################
# Listen
###############################################
while not done:
    time.sleep(0.1)

    #---------------------------------------------#
    #- new connections ---------------------------#
    #---------------------------------------------#
    user = GetNewConnection() 
    if user:
        userlist.append(user)
        print user.addr + " has connected"
    #---------------------------------------------#

    for user in userlist:
        try:
            data = user.sock.recv(1024)
            data = filter(lambda x: x >= " " and x <= "z", data)

            if data:
                if data == "shutdown":
                    done = 1
                else:
                    user.handleMsg(data)
        except:
            pass

###############################################
# Exit
###############################################
print "###############################################"
print "Closing all connections..."

for user in userlist:
    user.sock.close()

srv.close()

print "Done"
print "Exit"
print "###############################################"
