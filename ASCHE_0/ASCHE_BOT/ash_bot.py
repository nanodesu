import dcpp_client
import services

###############################################
# START 
###############################################
print "###############################################"
print "# dcpp_client v0.0.1                          #"
print "# (c) ASCHE 2008                              #"
print "###############################################"
###############################################
host  = "avangard.no-ip.org"
port  = 411
nick  = "ASH_BOT"
owner = "ASCHE_0"

client = dcpp_client.DCPP_CLIENT((host, port), nick)

###############################################
print "Connecting..."

if not client.connect():
    print "Error"
    exit()
print "Done"
print "###############################################"

###############################################
room_list = []

run = 1
while run:
    client.update_cache()

    while 1:
        data, user, msg = client.process_cache()

        if not data:
            break
        else:
            #exit
            if (user == owner) and (msg == "q!"):
                run = 0
                break

            #weather
            if msg == "->weather":
                client.send_to_user(user, services.GetWeather())

            #quotes
            elif msg == "->bash":
                client.send_to_user(user, services.GetBashQuote())
            elif msg == "->nyash":
                client.send_to_user(user, services.GetNyashQuote())
            elif msg == "->takto":
                client.send_to_user(user, services.GetTaktoQuote())

            #decode last
            elif (msg == "->en") or (msg == "->ru"):
                for room in room_list:
                    user_room, last_msg = room
                    if user_room == user:
                        client.send_to_user(user, services.Decode(last_msg, msg[2:]))

            #decode
            elif msg[:3] == "->e":
                client.send_to_user(user, services.Decode(msg[4:], "en"))
            elif msg[:3] == "->r":
                client.send_to_user(user, services.Decode(msg[4:], "ru"))

            #---------------------------------------------#
            #last_msg
            flag = 0

            for i in range(len(room_list)):
                user_room, last_msg = room_list[i]
                if user_room == user:
                    room_list[i] = user, msg
                    flag = 1
                    break

            if not flag:
                new_room = user, msg
                room_list.append(new_room)

###############################################
print "###############################################"
print "Exiting..."

del client

print "Done"
print "###############################################"
