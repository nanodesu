import socket, select

###############################################
# DCPP_CLIENT
###############################################
class DCPP_CLIENT:
    ST_DISCONNECTED = 0
    ST_CONNECTED = 1

    #---------------------------------------------#
    # constructor 
    #---------------------------------------------#
    def __init__(self, address, nick):
        self.address = address
        self.nick = nick

        self.cache = ""

        self.state = DCPP_CLIENT.ST_DISCONNECTED
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    #---------------------------------------------#
    # destructor 
    #---------------------------------------------#
    def __del__(self):
        self.disconnect()

    #---------------------------------------------#
    # connect
    #---------------------------------------------#
    def connect(self):
        if self.is_connected(): return 0 # if already connected

        try:
            self.sock.connect(self.address)
            self.state = DCPP_CLIENT.ST_CONNECTED
        except:
            return 0
        
        #self.recv()
        # TODO: process this!

        key = create_key(u"EXTENDEDPROTOCOL_verlihub").encode("utf-8")
        self.send("$Key %s|$ValidateNick %s|" %(key, self.nick))
        #self.recv()
        # TODO: process this!

        self.send("$Version 1,0091|$MyINFO $ALL %s desu$ $0$$0$|" %self.nick)

        return 1

    #---------------------------------------------#
    # disconnect
    #---------------------------------------------#
    def disconnect(self):
        if not self.is_connected(): return 0 # if already disconnected

        self.sock.close()
        self.state = DCPP_CLIENT.ST_DISCONNECTED

    #---------------------------------------------#
    # is_connected
    #---------------------------------------------#
    def is_connected(self):
        return (self.state == DCPP_CLIENT.ST_CONNECTED)

    #---------------------------------------------#
    # recv
    #---------------------------------------------#
    def recv(self):
        if not self.is_connected(): return "" # if disconnected

        inputready, outputready, exceptready = select.select([self.sock, ],[], [], 0.5)
        if inputready:
            try:
                return self.sock.recv(1024)
            except:
                pass
                return ""
                # TODO: process this!
        else:
            return ""

    #---------------------------------------------#
    # update_cashe
    #---------------------------------------------#
    def update_cache(self):
        self.cache += self.recv()

    #---------------------------------------------#
    # process_cashe
    #---------------------------------------------#
    def process_cache(self):
        pipepos = self.cache.find("|")
        
        if pipepos != -1:
            data = self.cache[:pipepos]
            self.cache = self.cache[pipepos+1:]

            #PM
            if data.find("$To:") != -1:
                p1 = data.find("From: ") + 6
                p2 = data.find(" $")
                user = data[p1:p2]

                p1 = data.find("> ") + 2
                msg = data[p1:]
            #MAIN
            elif data.find("> ") != -1:
                user = "#Main"

                p1 = data.find("> ") + 2
                msg = data[p1:]
            #OTHER
            else:
                return None, None, None

            return 1, user, msg
        else:
            return None, None, None

    #---------------------------------------------#
    # send
    #---------------------------------------------#
    def send(self, data):
        if not self.is_connected(): return 0 # if disconnected

        try:
            self.sock.send(data)
        except:
            pass
            # TODO: process this!

    #---------------------------------------------#
    # send_to_user
    #---------------------------------------------#
    def send_to_user(self, user, msg):
        # PM
        if user != "#Main":
            self.send("$To: %s From: %s $<%s> %s|" %(user, self.nick, self.nick, msg))
        # MAIN
        else:
            self.send("<%s> %s|" %(self.nick, msg))

###############################################
# create_key
###############################################
def create_key(lock):
    key = {}

    for i in xrange(1, len(lock)):
        key[i] = ord(lock[i]) ^ ord(lock[i-1])

    key[0] = ord(lock[0]) ^ ord(lock[len(lock)-1]) ^ ord(lock[len(lock)-2]) ^ 5

    for i in xrange(0, len(lock)):
        key[i] = ((key[i]<<4) & 240) | ((key[i]>>4) & 15)

    out = ''
    for i in xrange(0, len(lock)):
        out += unichr(key[i])

    out = out.replace(u'\0', u'/%DCN000%/').replace(u'\5', u'/%DCN005%/').replace(u'\44', u'/%DCN036%/')
    out = out.replace(u'\140', u'/%DCN096%/').replace(u'\174', u'/%DCN124%/').replace(u'\176', u'/%DCN126%/')

    return out
