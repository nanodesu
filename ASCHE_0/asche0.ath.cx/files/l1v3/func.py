from OpenGL.GL import *
from OpenGL.GLUT import *

###############################################
# drawText
###############################################
def drawText(str, x, y, z):
    glRasterPos3f(x, y, z)

    for character in str:
        if character == "\n":
            y -= 0.1
            glRasterPos3f(x, y, z)
        else:
            glutBitmapCharacter(GLUT_BITMAP_9_BY_15, ord(character))
