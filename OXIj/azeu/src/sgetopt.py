# -*- coding: utf-8 -*-
###
#This file is part of azeu project
#
#azeu is automatic upload manager
#Copyright (C) 2007 Jan Trofimov (OXIj)
#
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#You can contact author by email jan.trof@gmail.com
###

class SGetoptError(Exception):
	pass

def _find_num (ideas, num, value):
	for idea in ideas:
		if (idea[num] == value):
			return idea
	return None

#ideas = [['long_name', 'short_name', has_value], x N]
def sgetopt (arguments, ideas):
	optlist = []
	optargs = []

	argn = 0
	while (argn < len(arguments)):
		cur = arguments[argn]
		if (cur == '--'):
			for one in arguments[argn+1:]:
				optargs.append(one)
			break
		elif (cur[0:2] == '--'):
			cur = cur[2:]
			nameval = cur.split('=', 1)
			
			idea = _find_num(ideas, 0, nameval[0])
			if (idea == None):
				raise SGetoptError('Wrong option "' + nameval[0] + '"')

			value = ''
			if ( idea[2] == 0 ):
				if ( len(nameval) == 2 ):
					raise SGetoptError('Option "' + idea[0] + '" does not need parameter')
			elif ( idea[2] == 1 ):
				if ( len(nameval) == 2 ):
					value = nameval[1]
				elif ((argn + 1) < len(arguments)):
					argn += 1
					value = arguments[argn]
				else:
					raise SGetoptError('Option "' + idea[0] + '" need parameter')

			optlist.append([idea, value])

		elif (cur[0:1] == '-'):
			cur = cur[1:]

			found = 0
			startl = 0
			for l in xrange(1,len(cur)+1):
				idea = _find_num(ideas, 1, cur[startl:l])
				if (idea != None):
					found = 1

					value = ''
					if ( idea[2] == 1 ):
						if ( (l + 1) < len(cur) ):
							value=cur[l:]
						elif( (argn + 1) < len(arguments) ):
							argn += 1
							value = arguments[argn]
						else:
							raise SGetoptError('Option "' + idea[0] + '" need parameter')
				
					optlist.append([idea, value])

					if (value != ''):
						break

					startl = l

			if (not found):
				raise SGetoptError('Wrong option "' + cur + '"')
		else:
			optargs.append(cur)

		argn += 1

	return optlist, optargs
