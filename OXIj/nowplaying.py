#!/usr/bin/env python

from subprocess import *
import re

varibs = {}
varibs['artist'] = ""
varibs['album'] = ""
varibs['title'] = ""
varibs['player'] = ""

if (Popen("ps -e | grep mplayer", shell=True, stdout=PIPE).wait() == 0):
    varibs['player'] = "mplayer"
elif (Popen("ps -e | grep exaile", shell=True, stdout=PIPE).wait() == 0):
    varibs['player'] = "exaile"
elif (Popen("ps -e | grep quod", shell=True, stdout=PIPE).wait() == 0):
    varibs['player'] = "quodlibet"

if (varibs['player'] == "mplayer"):
    for line in Popen("ps ax | grep mplayer", shell=True, stdout=PIPE).communicate()[0].split("\n"):
        if (line.find("mplayer") != -1):
            res = re.search("\s*\d+\s*\S*\s*\S*\s*\S*\s*mplayer\s*(.+)", line)
            if res != None:
                varibs['title'] = res.groups()[0].split("/")[-1]
                break
elif (varibs['player'] == "exaile"):
    varibs['artist'] = Popen("exaile --get-artist", shell=True, stdout=PIPE).communicate()[0][:-1]
    varibs['album'] = Popen("exaile --get-album", shell=True, stdout=PIPE).communicate()[0][:-1]
    varibs['title'] = Popen("exaile --get-title", shell=True, stdout=PIPE).communicate()[0][:-1]
elif (varibs['player'] == "quodlibet"):
    varibs['title'] = Popen("quodlibet --print-playing", shell=True, stdout=PIPE).communicate()[0][:-1]


if (varibs['player']):
    out = "/me nowPlaying: "
    if (varibs['title']):
        out += varibs['title'] + " "

    if (varibs['artist']) and (varibs['album']):
        out += "(" + varibs['artist'] + " -  " + varibs['album'] + ") "
    elif (varibs['artist']) or (varibs['album']):
        out += "(" + varibs['artist'] + varibs['album'] + ") "

    out += "<" + varibs['player'] + ">"

    Popen("xsel", shell=True, stdin=PIPE).stdin.write(out)